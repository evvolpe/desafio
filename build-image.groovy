pipeline {
    agent any

    environment { 
        registry = "evvolpe/primeirodesafio" 
        registryCredential = 'dockerhub_id' 
        dockerImage = '' 
    }

    stages {
        stage('Cloning Repo') {
            steps {
                git url: 'https://gitlab.com/evvolpe/desafio.git'
                
            }
        }
        stage('Building Docker Image') {
            steps {
                script {
                    dockerImage = docker.build registry + ":$BUILD_NUMBER"
                }
            }
        }
        stage('Sending Image to Docker Hub') {
            steps {
                script {
                    docker.withRegistry( '', registryCredential ) { 
                    dockerImage.push() 
                    }
                }
            }
        }
        stage('Cleaning Jenkins Server') {
            steps {
                sh "docker rmi $registry:$BUILD_NUMBER"
            }
        }
        stage('Removing container') {
            steps {
                script{
                
                    def doc_containers = sh(returnStdout: true, script: 'docker container ps -aq').replaceAll("\n", " ") 
                    if (doc_containers) {
                        sh "docker stop ${doc_containers}"
                    }
                }    
            }
        }
        stage('Running container') {
            steps {
                sh "docker run -d -p 8082:80 evvolpe/primeirodesafio:$BUILD_NUMBER"
            }
        }
        
    }
}